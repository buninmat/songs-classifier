import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import numpy as np
import os
import getgpu
import dataset_loader


class RemoteDataset(Dataset):
    def __init__(self, url):
        self.url = url

    def __getitem__(self, item):
        data = dataset_loader(self.url)
        labels = torch.tensor(data['labels'])
        melspecs = torch.tensor(data['melspecs']).permute(2, 0, 1).unsqueeze(1)
        return melspecs[item], labels[item]

    def __len__(self):
        return self.melspecs.shape[2]


class Model(torch.nn.Module):

    def __init__(self):
        super().__init__()
        self.seq1 = self.init_conv_seq()

    def init_conv_seq(self, cin, cout, kernel=3, stride=1, padding=1):
        conv1 = nn.Conv2d(cin, cout, kernel, stride, padding)
        torch.nn.init.xavier_uniform_(conv1.weight)
        conv2 = nn.Conv2d(cin, cout, kernel, stride, padding)
        torch.nn.init.xavier_uniform_(conv2.weight)
        seq = nn.Sequential(
                conv1,
                nn.ReLu(),
                conv2,
                nn.ReLu()
                )
        return seq

    
    def weight_init(self):
        for lay in self.modules():
            if type(lay) in [torch.nn.Conv2d]:


    def forward(self, x):
        pass


class ContrastiveLossModule(torch.nn.Module):

    def __init__(self):
        pass
    

    def forward(self, out, labels):
        pass


if __name__ == '__main__':

    trn_url = 'ftp://147.32.124.180/windrive/songs-data/trn'
    val_url = 'ftp://147.32.124.180/windrive/songs-data/val'

    encoder = EncoderModule()
    loss = ContrastiveLossModule()
    
    print("Waiting for a GPU...")
    free_gpu = getgpu.wait_for_unused_gpu(10)
    print(f"Training on GPU {free_gpu}")
    dev = getgpu.get_device(free_gpu)

    model = Model()
    
    opt = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.99)
    
    trn_ds = RemoteDataset(trn_url)
    val_ds = MelSpectDataset(val_url)
    trn_loader = DataLoader(trn_ds, batch_size = 6, num_workers=1, shuffle = True)
    val_loader = DataLoader(val_ds, batch_size = 6, num_workers=1)

    for epoch in range(100):
        model.train()
        # TODO run lots of times, then calculate loss from output distribution
        for i, batch in enumerate(trn_ds):
            

        with torch.no_grad():
            model.eval()
            # TODO validate: pass segments of songs outside of training set
 
