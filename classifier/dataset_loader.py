import urllib.request
import re
import numpy as np
import io

def load_data_ftp(url):
    resp = urllib.request.urlopen(urllib.request.Request(url))
    ls = resp.read().decode('ascii')
    files = re.findall(' [A-Za-z0-9_-]+.npz', ls)
    for fname in files:
        print(fname)
        resp = urllib.request.urlopen(urllib.request.Request(f'{url}/{fname.strip()}'))
        print(resp)
        yield np.load(io.BytesIO(resp.read()))

    
