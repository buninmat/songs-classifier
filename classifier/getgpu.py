import torch
import os
import numpy as np


def get_device(gpu=0):  # Manually specify gpu
    if torch.cuda.is_available():
        device = torch.device(gpu)
    else:
        device = 'cpu'

    return device


def get_free_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
    index = np.argmax(memory_available[:-1])  # Skip the 7th card --- it is reserved for evaluation!!!

    return index  # Returns index of the gpu with the most memory available


def wait_for_unused_gpu(req_pos_checks):
    gpu_num = -1
    pos_checks = 0
    while pos_checks != req_pos_checks:
        os.system("nvidia-smi -q -d Utilization | grep Gpu | grep -o '[0-9]\\+' >tmp")
        usage = [int(x) for x in open('tmp', 'r').readlines()][:-1]
        num = -1 if 0 not in usage else usage.index(0)
        if num == gpu_num and num != -1:
            pos_checks += 1
        else:
            pos_checks = 0
        gpu_num = num
    return gpu_num
