import os
import numpy as np
import librosa

class MelSpecDatasetCreator:
    """
        Creates dataset from mp3 files organized as
        <dir>/[Song1, Song2...]/[track1.mp3, track2.mp3...]

        Dataset is created as follows:

        melspec: array of spectograms, (n_pitches, segment_len, n)
        names: names of songs
        labels: indices into names array, (n,)
        cover_indices: indices of covers as they are read from the directory
    """

    # standard sampling frequency, Hz
    sampling_freq = 22050

    def __init__(self, segment_dur_sec=20, hop_dur_sec=0.02):
        self.segment_dur_sec  = segment_dur_sec
        self.hop_dur_sec = hop_dur_sec

    def create(self, data_dir, save_dir, save_name, max_size_bytes=np.inf):
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        file_ind = 0
        for melspecs, labels, cover_indices, names in self.read_melspecs(data_dir, max_size_bytes):
            filename = os.path.join(save_dir, save_name + f'_{file_ind}' if file_ind != 0 else '') 
            melspecs = np.transpose(np.array(melspecs), (1, 2, 0))
            print('\nSaving to', filename, '.npz', 'melspecs', melspecs.shape, '\n')
            np.savez(filename,\
                    melspecs=melspecs,\
                    labels=np.array(labels),\
                    cover_indices=np.array(cover_indices),\
                    names=np.array(names, dtype=str))
            file_ind += 1


    def read_melspecs(self, data_dir, max_size_bytes):
        walk = os.walk(data_dir)
        names = next(walk)[1]
        labels = []
        cover_indices = []
        melspecs = []
        for i, dir_data in enumerate(walk):
            dirp, subd, files = dir_data
            print(dirp)
            for cov_ind, fname in enumerate(files):
                if os.path.splitext(fname)[1] != '.mp3':
                    continue
                print('\n', '--',fname, '\n')
                y, sr = librosa.load(os.path.join(dirp, fname))
                assert(sr == self.sampling_freq)
                #compute mel spectrum
                hop_length = int(np.round(self.sampling_freq * self.hop_dur_sec))
                melspec = librosa.feature.melspectrogram(y, sr, hop_length=hop_length)
                melspec = librosa.power_to_db(melspec, ref=np.max)
                n_pitch, n_samp = melspec.shape
                assert(n_pitch == 128)
                # split into segmenta of equal length
                seg_len = int(np.round(self.segment_dur_sec / self.hop_dur_sec))
                segnum = n_samp // seg_len
                #create segments
                for j in range(segnum):
                    labels.append(i)
                    cover_indices.append(cov_ind)
                    segment = melspec[:, j * seg_len : (j + 1) * seg_len]
                    melspecs.append(segment)
                #append last segment with zeros if it is longet than 0.2 segment
                last_seg_len = n_samp % seg_len
                if last_seg_len > seg_len / 5:
                    zeros = np.zeros((n_pitch, seg_len - last_seg_len))
                    last_seg = np.hstack((melspec[:,-last_seg_len:], zeros))
                    labels.append(i)
                    cover_indices.append(cov_ind)
                    melspecs.append(last_seg)
                
                size = sum(map(lambda seg: seg.size * 4, melspecs))
                if size >= max_size_bytes:
                    yield melspecs, labels, cover_indices, names
                    melspecs.clear()
                    labels.clear()
                    cover_indices.clear()
        if melspecs:
            yield melspecs, labels, cover_indices, names


    def visualize(self, npz_path, label, cov_ind):
        import matplotlib.pyplot as plt
        import seaborn as sb
        data = np.load(npz_path)
        melspecs = data['melspecs']
        labels = data['labels']
        plt.figure()
        segs = melspecs[:,:, labels == label]
        pitch, seg_len, n = segs.shape
        sb.heatmap(segs.reshape(pitch, -1))
        for i in range(n):
            x = i * seg_len
            plt.plot([x, x], [-1000, 1000], 'g--')
        plt.show()
            

if __name__ == '__main__':
    dc = MelSpecDatasetCreator()
    dc.create('data', os.getcwd(), 'test', 5e9) 
