from __future__ import unicode_literals
import youtube_dl


ydl_opts = {
    'outtmpl' : "default",
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        #'preferredquality': '192',
        }],
    }

links_path = "data/links.txt"
errors_path = "data/error_links.txt"
links = open(links_path, "r")
error_file = open(errors_path, "w")

error_file.write("links that could not be downloaded")

folder = "none"
links_processed = 0
in_clique_num = 0
errors = 0
while True:

    line = links.readline()

    if len(line) == 0:
        break

    if line[0] == "%":
        name = line.strip("%").lstrip().strip("\n")
        in_clique_num = 0
        print(name)
    else:

        ydl_opts ['outtmpl'] = "data/" + name + "/" + str(in_clique_num)

        try:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([line])
        except:
            print("couldnt download this")
            print(name)
            print(line)
            error_file.write(name)
            error_file.write(line)
            errors += 1

        in_clique_num += 1
        links_processed += 1
        print(line, "links_processed: ", links_processed)


