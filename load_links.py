import json
import requests




dataset_path = "dataset_raw/dataset.txt"
links_path = "dataset_raw/links.txt"



input_file = open(dataset_path, "r")
output_file = open(links_path, "w")

link_start = "https://secondhandsongs.com/performance/"
link_end = "?format=json"

clique_name = None
num_cliques = 0
missing_links = 0
present_links = 0


while True:

    line = input_file.readline()

    if len(line) == 0:
        break

    if line[0] == "#":
        continue

    elif line [0] == "%":
        clique_name = line.split(",")[-1]

        output_file.write("%")
        output_file.write(clique_name)

        num_cliques += 1
        print(clique_name, "num: ", num_cliques)


    else:
        id = line.split("<SEP>")[-1].strip("\n")
        if id != -1:
            shs_link = link_start + id + link_end
            json_file = requests.get(shs_link)
            parsed_json = json.loads(json_file.text)

            if "error" in parsed_json:
                missing_links += 1
            else:
                external_uri = parsed_json["external_uri"]
                if len(external_uri) == 0:
                    missing_links += 1
                else:
                    yt_link = external_uri[0]["uri"]
                    present_links += 1
                    print(yt_link, "num: ", present_links)
                    output_file.write(yt_link)
                    output_file.write("\n")
        else:
            missing_links += 1

print("finished")
print("cliques: ",num_cliques)
print("present_links ", present_links)
print("missing_links ", missing_links)